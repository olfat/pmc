<?php /* Template Name: Specialities */ 

get_header();
global $road9;
$image = $road9['specialities-image']['url'];
?>
<div class="container-fluid p-5">
    <?php echo '<p class="text-uppercase page-sub-title">'.$obj -> name. '</p>'; ?>
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>
<?php
$args = array(
    'post_type' => 'speciality',
    'posts_per_page' => -1,
    'orderby' => 'ID',
    'order' => 'ASC',
    
);
$query = new WP_Query( $args );
		$posts_ids = array();
			// Start the Loop.
			while (  $query->have_posts() ) :  $query->the_post();
				$posts_ids[] = get_the_ID();
			endwhile;
			wp_reset_postdata();

		?>
		
		<section id="tabs-page">
			<div class="row">
				<div class="col-md-4">
					<!-- Tabs -->
					<div class="nav flex-column nav-pills nav-tabs" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<?php
						// Start the Loop.
						foreach($posts_ids as $post_id)	{
							?>
		<a class="nav-link" id="v-pills-<?php echo $post_id;?>-tab" href="#tab-<?php echo $post_id;?>" data-tab="tab-<?php echo $post_id;?>"><img src="<?php echo get_the_post_thumbnail_url($post_id);?>" alt="" class="px-2 pb-2"><?php echo get_the_title($post_id);?></a>
							<?php
						}
						?>
					</div>
				</div>
			
				<div class="col-md-8">
					<div class="container">
						<div class="tab-content" id="v-pills-tabContent">
						<?php
							// Start the Loop.
							foreach($posts_ids as $post_id)	{				
								?>						
								<div class="tab-pane" id="tab-<?php echo $post_id; ?>">			
									<img src="<?php echo $image; ?>" alt="">		
									<div class="overlay"></div>
									<h5 class="h5 pt-5"><?php echo get_the_title($post_id); ?></h5>
									<p class="small para"><?php echo get_post_field('post_content', $post_id);?></p>

                    <h6 class="h6 pt-5"> <?php echo get_the_title($post_id);?> Doctors</h6>
                    <section id="doctors">
                        <div class="container">
                            <div class="row">
                                <div class="owl-carousel owl-theme owl-1">
                                <?php
                                $args = array(
                                    'post_type' => 'doctor',
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        'relation' => 'OR',
	                                array(
                                        'key'      => 'mb_speciality',
                                        'value'    => $post_id,
                                        'compare'       => 'LIKE'
                                    )  )                             
                                );
                                $query = new WP_Query( $args );
                                while ( $query->have_posts() ) :
                                    $query->the_post();
                                    ?>                                                
                                        <div class="card text-white">
                                        <?php if(has_post_thumbnail()){ ?>
                                            <img class="card-img" src="<?php the_post_thumbnail_url() ; ?>" alt="Card image">
                                            <?php  }else{ ?>
                                                <img class="card-img" src="http://via.placeholder.com/360x640" alt="Card image">
                                            <?php } ?>
                                            <div class="card-img-overlay">
                                                <a href="#" class="card-link text-uppercase"><span class="card-link-text">More about doctors</span><span class="card-link-arrow text-uppercase"><img src="./img/arrow.png" alt="arrow" class="px-2"></span></a>
                                                <div class="card-text-overlay">
                                                    <h5 class="card-title font-weight-bold"><?php the_title(); ?></h5>
                                                    <p class="card-text small"><?php echo get_the_title($post_id);?></p>
                                                </div>

                                            </div>
                                        </div>  
                                <?php endwhile;
                                wp_reset_postdata();?>       
                            </div>
                        </div>  
                        </div>  
                    </section>
									
								</div>
								<?php					
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</section>
		



        
		
<?php 
get_footer();
