// ############ Preloader
var preloader;

function preload(opacity) {
    // back to <=0 before production
    if (opacity <= 0) {
        showContent();
    } else {
        preloader.style.opacity = opacity;
        window.setTimeout(function () {
            preload(opacity - 0.05)
        }, 100);
    };
};

function showContent() {
    preloader.style.display = 'none';
    document.getElementById('body-wrapper').style.visibility = 'visible';
    document.getElementById('body-wrapper').style.opacity = '1';
};

document.addEventListener("DOMContentLoaded", function () {
    preloader = document.getElementById('preloader');
    preload(1);
});
// ############# End of Preloader


// Menu
$(".first-main").clone().appendTo(".first-main-res");

$(".second-main").clone().appendTo(".second-main-res");
//menu overlay
//Toggle main menu open and close
function openMenu() {
    // document.getElementById('res-menu').style.display = 'block';
    document.getElementById('res-menu').style.visibility = 'visible';
    document.getElementById('res-menu').style.opacity = '1';
};

function closeMenu() {
    document.getElementById('res-menu').style.visibility = 'hidden';
    document.getElementById('res-menu').style.opacity = '0';
};

// Toggle responsive sub-menu open and close
let link = document.getElementsByClassName('res-dropdown-link');
let content = document.getElementsByClassName('res-dropdown-content');
let closeContent = document.getElementsByClassName('res-close');

for (let i = 0; i < link.length; i++) {
    //open the content related to the link
    link[i].addEventListener('click', show);

    function show() {
        content[i].style.visibility = 'visible';
        content[i].style.opacity = '1';
    };

    // Close content
    closeContent[i].addEventListener('click', hide);

    function hide() {
        content[i].style.visibility = 'hidden';
        content[i].style.opacity = '0';
    };
};
 
 $('#owl-doctors').owlCarousel({
    rtl: true,
    loop:false,
    margin:16,
    dots:true,
    responsive:{
        0:{
            loop: true,
            items:1,
            stagePadding: 50
        },
        600:{
            items:2,
            stagePadding: 50
        },
        768:{
            items:3,
            stagePadding: 50
        },
        992:{
            items:3
        }
    }
})


  $('#owl-offers').owlCarousel({
    rtl: true,
    loop:false,
    margin:16,
    dots:true,
    responsive:{
      0:{
            loop: true,
            items:1,
            stagePadding: 50
        },
        600:{
            items:2,
            stagePadding: 50
        },
        768:{
            items:3,
            stagePadding: 50
        },
        992:{
            items:3
        }
    }
})

$('#owl-news').owlCarousel({
    rtl: true,
    loop:false,
    margin:16,
    dots:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1,
            stagePadding: 50
        },
        750:{
            items:2,
        },
        1000:{
            items:2
        }
    }
})

$('#owl-insurance').owlCarousel({
    rtl: true,
    loop:true,
    margin: 70,
    dots:false,
    nav: true,
    responsive:{
      0:{
            margin: 30,
            items:3,
            stagePadding: 50
        },
        600:{
            margin: 30,
            items:4,
            stagePadding: 50
        },
        768:{
            margin: 30,
            items:5,
            stagePadding: 50
        },
        992:{
            margin: 30,
            items:7,
            stagePadding: 50
        }
    },
    navText : ["<span><img src='./img/left-arrow-grey.png' alt='prev'></span>","<span><img src='./img/right-arrow-grey.png' alt='next'></span>"]

})

$('#owl-doctors-specialties').owlCarousel({
    rtl: true,
	loop:false,
	margin:16,
	dots:true,
	responsive:{
			0:{
					loop: true,
					items:1,
					stagePadding: 50
			},
			600:{
					items:2,
			},
			768:{
					items:2,
			},
			992:{
					items:2,

			}
	}
});

// about-us page images animation

var animateHTML = function() {
    var elems;
    var windowHeight;
    function init() {
        elems = document.querySelectorAll('.img-cont');
        windowHeight = window.innerHeight;
        addEventHandlers();
        checkPosition();
    };
    function addEventHandlers() {
        window.addEventListener('scroll', checkPosition);
        window.addEventListener('resize', init);
    };
    function checkPosition() {
        for (var i = 0; i < elems.length; i++) {
            var positionFromTop = elems[i].getBoundingClientRect().top;
            if (positionFromTop - windowHeight <= 0) {
                elems[i].className = elems[i].className.replace(
                    'hidden',
                    'animate'
                );
            };
        };
    };
    return {
        init: init
    };
};
animateHTML().init();

// End of about-us page animation

// Add arrow in case of small screen sizes

    if ($(window).width() < 992) {
        $('<p class="text-light mx-5 mt-5 dropdown-title-text"></p>').prependTo('.dropdown-menu');
    }

// End Add arrow 

// Single doctors div height

if($('#doctors-single .card-body').height() > 650) {
    $('#doctors-single .card-img').css('border-radius: 22px 0 0 0');
}
                  